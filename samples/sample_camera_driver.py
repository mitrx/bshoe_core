import drivers.camera_driver as camera
import bshoe_core.constants as constants


def test_get_image():
    camera_driver = camera.CameraDriver(constants.DEVICE_CONFIG_DIR, 1)

    image = camera_driver.fetch_image()
    print(type(image))

    with open('image.bmp', 'wb') as f:
        f.write(image)


if __name__ == '__main__':
    test_get_image()
