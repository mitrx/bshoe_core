import time

from bshoe.loop_thread import LoopThread


def test_loop_thread():
    def step_function():
        pass

    thread = LoopThread(step_function)

    print('is_running?', thread.is_running())
    print('is_alive?', thread.is_alive())
    print('is_paused?', thread.is_paused(), '\n')

    thread.start()
    print('thread.start()')
    time.sleep(1)

    print('is_running?', thread.is_running())
    print('is_alive?', thread.is_alive())
    print('is_paused?', thread.is_paused(), '\n')

    thread.pause()
    print('thread.pause()')

    print('is_running?', thread.is_running())
    print('is_alive?', thread.is_alive())
    print('is_paused?', thread.is_paused(), '\n')

    thread.request_termination()
    print('thread.terminate()')

    print('is_running?', thread.is_running())
    print('is_alive?', thread.is_alive())
    print('is_paused?', thread.is_paused(), '\n')

    thread.join()
    print('thread.join()')

    print('is_running?', thread.is_running())
    print('is_alive?', thread.is_alive())
    print('is_paused?', thread.is_paused(), '\n')


if __name__ == '__main__':
    test_loop_thread()
