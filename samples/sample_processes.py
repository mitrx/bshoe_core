
import multiprocessing as mp

import threading as th

import time


class ProcessChild(mp.Process):

    def __init__(self):
        super().__init__()

    def run(self, *args, **kwargs):
        some_thread = th.Thread(target=self.endless_hello)
        another_thread = th.Thread(target=self.ten_times_hello,
                                   args=(some_thread,))
        some_thread.start()
        another_thread.start()
        print('waiting another thread')
        some_thread.join()
        another_thread.join()
        print('another thread joined!')

    @staticmethod
    def endless_hello():
        for i in range(8):
            print('Hello!')
            time.sleep(1)

    @staticmethod
    def ten_times_hello(some_thread):
        for i in range(5):
            print('Privet!')
            time.sleep(1)


def test_child_process_threads():
    process_child = ProcessChild()
    print('exitcode =', process_child.exitcode)
    print('start')
    process_child.start()
    print('exitcode =', process_child.exitcode)
    print('started')
    print('exitcode =', process_child.exitcode)
    process_child.join()
    print(process_child.exitcode)
    print('finished')


def thread_loop_test():
    def my_loop_thread():
        for i in range(5):
            print('Loop!')
            time.sleep(1)

    thread = th.Thread(target=my_loop_thread)
    thread.start()
    thread.join()
    print('joined')


def test_termination():

    def my_target():
        while True:
            print('Hello!')
            time.sleep(1)

    def my_another_target():
        while True:
            print('Privet!')
            time.sleep(0.5)

    my_process = mp.Process(target=my_target)
    another_process = mp.Process(target=my_another_target)

    my_process.start()
    another_process.start()

    print('waiting...')
    time.sleep(5)
    my_process.terminate()
    print('my_process has terminated')
    another_process.terminate()
    print('terminated')


if __name__ == '__main__':
    test_child_process_threads()
