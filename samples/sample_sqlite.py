import multiprocessing as mp

from bshoe_core.db_utils import (
    register_device, register_package_type, store_package, retrieve_device,
    retrieve_package_type, retrieve_package, retrieve_packages, delete_package,
    delete_packages
)

import json


def test_register_device():
    register_device('my_device')


def test_register_package_type():
    register_package_type('my_data_type3', SourceTypeEnum.INTEGER)


def test_store_package():
    store_package(1, 1, ('string', 1.2, 5, True))


def test_retrieve_package_type_id():
    type_id = retrieve_package_type_id('my_data_type3')
    print(type_id)


def test_retrieve_device_id():
    device_id = retrieve_device_id('my_device')
    print(device_id)


def test_store_device_data():
    store_package('my_device', 'my_data_type3', {
        'a': 1,
        'b': 2
    })

def test_retrieve_package():
    print(retrieve_package('2'))


def test_delete_package():
    print(delete_package('3'))


def test_get_source_type_of_package_data():
    package = retrieve_package(4)
    source_type = retrieve_source_type(package[4])
    print(source_type)


def test_binary_package():
    package_id = store_package(1, 3, 'hello')
    package = retrieve_package(package_id)
    print(package)


def test_retrieve_packages():
    packages = retrieve_packages()
    print(packages)


if __name__ == '__main__':
    # test_register_device()
    # test_register_package_type()
    # test_store_device_data()
    # test_retrieve_package()
    # test_delete_package()
    # test_store_package()
    # test_get_source_type_of_package_data()
    # test_binary_package()
    test_retrieve_packages()
