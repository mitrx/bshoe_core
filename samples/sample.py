"""Basic test of the system"""

import multiprocessing as mp

from bshoe_core.event_processor import EventProcessor

import os

import datetime

from bshoe_core.constants import DB_PATH

from bshoe_core.db_utils import *

def sample_event_processor():
    input_queue = mp.Queue()
    output_queue = mp.Queue()
    ep = EventProcessor(input_queue, output_queue, name='EventProcessor1')

    ep.start()
    print(ep.name, 'started')
    ep.request_halt()
    print(ep.name, 'halt requested')
    ep.join()
    print(ep.name, 'joined')

    input_queue = mp.Queue()
    output_queue = mp.Queue()
    new_ep = EventProcessor(input_queue, output_queue, name='EventProcessor2')

    new_ep.start()
    print(new_ep.name, 'started')
    new_ep.request_halt()
    print(new_ep.name, 'halt requested')
    new_ep.join()
    print(new_ep.name, 'joined')


def sample_datetime_comparison():
    print(os.path.curdir)

    t1 = '2013-01-12 15:27:43'
    t2 = '2013-01-12 15:27:44'
    dt_format = '%Y-%m-%d %H:%M:%S'
    dt1 = datetime.datetime.strptime(t1, dt_format)
    dt2 = datetime.datetime.strptime(t2, dt_format)
    print(dt1 < dt2)
    print(dt1 > dt2)
    print(dt1 == dt2)


def sample_db_utils():
    register_device(DB_PATH, 'my_device')
    register_device(DB_PATH, 'my_another_device')
    register_package_type(DB_PATH, 'my_data_type1',
                          SourceTypeEnum.JSON)
    binary_id = register_package_type(DB_PATH, 'my_data_type3',
                                      SourceTypeEnum.BINARY)
    package_id = store_package(DB_PATH, 1, binary_id, b'hello')


if __name__ == '__main__':
    sample_db_utils()
