#!/usr/bin/env python

from setuptools import setup

with open('README.rst', 'r') as f:
    long_description = f.read()

setup(
    name='bshoe_core',
    description='The components of Bast Shoe project',
    long_description=long_description,
    author='mitrx',
    url='https://mitrx@bitbucket.org/mitrx/bshoe.git',
    author_email='d.alxv@yandex.com',
    version='0.1',
    packages=['bshoe_core', 'drivers'],
    entry_points={'console_scripts': ['run = bshoe_core.system:run',
                                      'init = bshoe_core.system:init']},
    test_suite='nose2.collector.collector',
    tests_require=['nose2'],
    license='GPLv3',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.6'
    ]
)
