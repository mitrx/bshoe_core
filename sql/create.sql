CREATE TABLE IF NOT EXISTS device(
  device_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  device_name TEXT NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS package_type(
  type_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  type_name TEXT NOT NULL UNIQUE,
  source_type TEXT NOT NULL CHECK (source_type IN
              ('string', 'integer', 'real', 'binary', 'json'))
);

CREATE TABLE IF NOT EXISTS data_package(
  package_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  package_data TEXT NOT NULL,
  create_datetime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  device_id INTEGER NOT NULL REFERENCES device(device_id),
  type_id INTEGER NOT NULL REFERENCES package_type(type_id)
);
