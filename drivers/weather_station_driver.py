"""
Weather station wrapper draft
Python 3.5


Weather station commands:
OPEN    - OPEN CONSOLE
CLOSE   - CLOSE CONSOLE
HELP    - LIST OF COMMANDS
RESET   - SYSTEM RESET
LINK    - COMMUNICATION LINK
POLL    - POLL SENSORS IMMEDIATELY
HIST    - MEASUREMENT HISTORY
INTV    - HISTORY INTERVAL [min]
DATE    - SYSTEM DATE [YYYY[-MM[-DD]]]
TIME    - SYSTEM TIME [HH[:MM[:SS]]]
CONF    - SYSTEM PARAMETERS
STAT    - SYSTEM STATUS
WGAUGE  - WIND GAUGE DATA [Hz]
WVANE   - WIND VANE DATA [degree]
AIR     - AIR DATA
SURF    - SURFACE TEMPERATURE DATA
WIND    - WIND DATA
PRECIP  - PRECIPITATION DATA
OPTEYE  - OPTEYE DATA
ROAD    - ROAD CONDITION DATA
BOOT    - BOOT COUNTERS
PSMON   - POWER SUPPLY MONITOR STATE
CAL     - CALIBRATION
VER     - SOFTWARE VERSION
ID      - SYSTEM ID
NAME    - SYSTEM NAME
APNSERV - APN SERVER
APNUN   - APN USERNAME
APNPW   - APN PASSWORD
FTPPUT  - UPLOAD DATA TO FTP SERVER
FTPAPUT - FTP AUTO UPLOAD [ON/OFF]
FTPSERV - FTP SERVER
FTPUN   - FTP USERNAME
FTPPW   - FTP PASSWORD
FTPPATH - FTP REMOTE DIRECTORY
"""

import telnetlib


class WeatherStationCmdError(Exception):
    pass


class WeatherStationDriver(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.connect = telnetlib.Telnet(self.host, self.port)
        self.available_cmds = [
            'OPEN', 'CLOSE', 'HELP', 'RESET', 'LINK', 'POLL', 'HIST', 'INTV',
            'DATE', 'TIME', 'CONF', 'STAT', 'WGAUGE', 'WVANE', 'AIR', 'SURF',
            'WIND', 'PRECIP', 'OPTEYE', 'ROAD', 'BOOT', 'PSMON', 'CAL', 'VER',
            'ID', 'NAME', 'APNSERV', 'APNUN', 'APNPW', 'FTPPUT', 'FTPAPUT',
            'FTPSERV', 'FTPUN', 'FTPPW', 'FTPPATH'
        ]

        self.start_session_cmd = b'\x1b'
        self.end = '\n'

    def send_cmd(self, cmd):
        if cmd not in self.available_cmds:
            raise WeatherStationCmdError(
                "{cmd} is not supported".format(cmd=cmd)
            )
        self.connect.get_socket().send(self.start_session_cmd)
        self.connect.write(bytes(''.join((cmd, self.end)).encode('ascii')))
        return self.connect.read_all()


def test():
    ms = WeatherStationDriver(host="213.87.13.211", port="4001")
    print(ms.send_cmd("POLL"))


if __name__ == '__main__':
    test()
