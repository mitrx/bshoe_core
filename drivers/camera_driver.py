import requests
import bshoe_core.constants
import json
import os

from typing import Any, Optional


class CameraDriver(object):
    FS_NAME = 'camera'

    def __init__(self, config_dir: str, id: Optional[int] = 1):
        self.id = id
        self.config = None
        self.config_fname = '{}-{}.json'.format(CameraDriver.FS_NAME, self.id)
        self.config_fpath = os.path.join(config_dir, self.config_fname)

    def load_config(self) -> None:
        with open(self.config_fpath, 'r') as config_file:
            self.config = json.loads(config_file.read())

    def fetch_image(self) -> bytes:
        self.load_config()
        res = requests.get(
            'http://{}:{}/axis-cgi/bitmap/image.bmp'.format(
                self.config['host'], self.config['port']),
            auth=(self.config['login'], self.config['password'])
        )
        if res.status_code != requests.status_codes.codes.ok:
            raise ImageFetchError('Image could not be fetched')
        return res.content


class ImageFetchError(RuntimeError):
    def __init__(self, msg: str = None):
        super(ImageFetchError, self).__init__(msg)
