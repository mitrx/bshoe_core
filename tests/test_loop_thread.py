import unittest

from bshoe_core.loop_thread import LoopThread


class LoopThreadTestCase(unittest.TestCase):

    def setUp(self):
        def step_function():
            pass
        self.thread = LoopThread(step_function)

    def tearDown(self):
        if self.thread.is_alive():
            self.thread.request_termination()
            self.thread.join()

    def test_init(self):
        self.assertFalse(self.thread.is_running())
        self.assertFalse(self.thread.is_alive())
        self.assertFalse(self.thread.is_paused())

    def test_start(self):
        self.thread.start()

        self.assertTrue(self.thread.is_running())
        self.assertTrue(self.thread.is_alive())
        self.assertFalse(self.thread.is_paused())

    def test_pause(self):
        self.thread.pause()

        self.assertFalse(self.thread.is_running())
        self.assertFalse(self.thread.is_alive())
        self.assertFalse(self.thread.is_paused())

    def test_resume(self):
        self.thread.resume()

        self.assertFalse(self.thread.is_running())
        self.assertFalse(self.thread.is_alive())
        self.assertFalse(self.thread.is_paused())

    def test_terminate(self):
        self.assertRaises(RuntimeError, self.thread.request_termination)

    def test_start_pause(self):
        self.thread.start()
        self.thread.pause()

        self.assertFalse(self.thread.is_running())
        self.assertTrue(self.thread.is_alive())
        self.assertTrue(self.thread.is_paused())

    def test_start_terminate(self):
        self.thread.start()
        self.thread.request_termination()

        self.assertFalse(self.thread.is_paused())

    def test_start_resume(self):
        self.thread.start()
        self.thread.resume()

        self.assertTrue(self.thread.is_running())
        self.assertTrue(self.thread.is_alive())
        self.assertFalse(self.thread.is_paused())

    def test_start_terminate_join(self):
        self.thread.start()
        self.thread.request_termination()
        self.thread.join()

        self.assertFalse(self.thread.is_running())
        self.assertFalse(self.thread.is_alive())
        self.assertFalse(self.thread.is_paused())

    def test_start_pause_resume(self):
        self.thread.start()
        self.thread.pause()
        self.thread.resume()

        self.assertTrue(self.thread.is_running())
        self.assertTrue(self.thread.is_alive())
        self.assertFalse(self.thread.is_paused())

    def test_start_pause_resume_terminate_join(self):
        self.thread.start()
        self.thread.pause()
        self.thread.resume()
        self.thread.request_termination()
        self.thread.join()

        self.assertFalse(self.thread.is_running())
        self.assertFalse(self.thread.is_alive())
        self.assertFalse(self.thread.is_paused())

    def test_start_pause_terminate_join(self):
        self.thread.start()
        self.thread.pause()
        self.thread.request_termination()
        self.thread.join()

        self.assertFalse(self.thread.is_running())
        self.assertFalse(self.thread.is_alive())
        self.assertFalse(self.thread.is_paused())


if __name__ == '__main__':
    unittest.main()
