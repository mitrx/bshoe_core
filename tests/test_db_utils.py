
import unittest

import os

from bshoe_core.constants import DB_PATH

import datetime

from bshoe_core.db_utils import (
    register_device, register_package_type, store_package, retrieve_device,
    retrieve_package_type, retrieve_package, retrieve_packages, delete_package,
    delete_packages, SourceTypeEnum, init
)

import sqlite3 as sqlite

TEST_DB_PATH = os.path.join(os.path.dirname(DB_PATH), 'test_db.db')

DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'


class DbUtilsTestCase(unittest.TestCase):

    def setUp(self):
        if os.path.exists(TEST_DB_PATH):
            os.remove(TEST_DB_PATH)
        init(TEST_DB_PATH)

    def tearDown(self):
        pass

    def test_register_device_ok(self):
        id = register_device(TEST_DB_PATH, 'my_device')
        self.assertEqual(id, 1)

    def test_register_device_with_existing_name(self):
        register_device(TEST_DB_PATH, 'my_device')
        self.assertRaises(sqlite.IntegrityError, register_device,
                          TEST_DB_PATH, 'my_device')

    def test_retrieve_device_record(self):
        id = register_device(TEST_DB_PATH, 'my_device')
        (device_id, device_name) = retrieve_device(TEST_DB_PATH, 'my_device')
        self.assertEqual(device_id, id)
        self.assertEqual(device_name, 'my_device')
        (device_id, device_name) = retrieve_device(TEST_DB_PATH, id)
        self.assertEqual(device_id, id)
        self.assertEqual(device_name, 'my_device')

    def test_register_package_type_ok(self):
        id = register_package_type(TEST_DB_PATH, 'my_data_type3',
                                   SourceTypeEnum.INTEGER)
        self.assertEqual(id, 1)

    def test_register_package_type_with_existing_name(self):
        register_package_type(TEST_DB_PATH, 'my_data_type3',
                              SourceTypeEnum.INTEGER)
        self.assertRaises(sqlite.IntegrityError, register_package_type,
                          TEST_DB_PATH, 'my_data_type3',
                          SourceTypeEnum.INTEGER)

    def test_retrieve_package_type_ok(self):
        id = register_package_type(TEST_DB_PATH, 'my_data_type3',
                                   SourceTypeEnum.INTEGER)
        (type_id, type_name, source_type) = retrieve_package_type(
            TEST_DB_PATH, id)
        self.assertEqual(type_id, id)
        self.assertEqual(type_name, 'my_data_type3')
        self.assertEqual(source_type, SourceTypeEnum.INTEGER.value)
        (type_id, type_name, source_type) = retrieve_package_type(
            TEST_DB_PATH, 'my_data_type3')
        self.assertEqual(type_id, id)
        self.assertEqual(type_name, 'my_data_type3')
        self.assertEqual(source_type, SourceTypeEnum.INTEGER.value)

    def test_store_package(self):
        device_id = register_device(TEST_DB_PATH, 'my_device')
        type_id = register_package_type(TEST_DB_PATH, 'my_data_type3',
                                        SourceTypeEnum.JSON)
        id = store_package(TEST_DB_PATH, device_id, type_id,
                           ('string', 1.2, 5, True))
        self.assertEqual(id, 1)

    def test_binary_package(self):
        register_device(TEST_DB_PATH, 'my_device')
        register_package_type(TEST_DB_PATH, 'my_data_type3',
                              SourceTypeEnum.BINARY)
        package_id = store_package(TEST_DB_PATH, 1, 1, b'hello')
        package = retrieve_package(TEST_DB_PATH, package_id)
        self.assertEqual(package[1], b'hello')

    def test_delete_package(self):
        register_device(TEST_DB_PATH, 'my_device')
        register_package_type(TEST_DB_PATH, 'my_data_type3',
                              SourceTypeEnum.BINARY)
        store_package(TEST_DB_PATH, 1, 1, b'hello')
        store_package(TEST_DB_PATH, 1, 1, b'hello again')
        deleted = delete_package(TEST_DB_PATH, 1)
        self.assertTrue(deleted)
        deleted = delete_package(TEST_DB_PATH, 1)
        self.assertFalse(deleted)
        deleted = delete_package(TEST_DB_PATH, 2)
        self.assertTrue(deleted)

    def test_delete_packages(self):
        register_device(TEST_DB_PATH, 'my_device')
        register_device(TEST_DB_PATH, 'my_another_device')
        register_package_type(TEST_DB_PATH, 'my_data_type1',
                              SourceTypeEnum.JSON)
        register_package_type(TEST_DB_PATH, 'my_data_type3',
                              SourceTypeEnum.BINARY)
        store_package(TEST_DB_PATH, 1, 2, b'hello')
        store_package(TEST_DB_PATH, 2, 2, b'hello again')
        store_package(TEST_DB_PATH, 1, 1, [1, 2, 3])
        self.assertEqual(delete_packages(TEST_DB_PATH, package_type=1), 1)
        self.assertEqual(delete_packages(TEST_DB_PATH, package_type=2), 2)

    def test_retrieve_package(self):
        register_device(TEST_DB_PATH, 'my_device')
        register_package_type(TEST_DB_PATH, 'my_data_type3',
                              SourceTypeEnum.JSON)
        id = store_package(TEST_DB_PATH, 1, 1, ('string', 1.2, 5, True))

        (package_id, package_data, create_dt,
         device_id, type_id) = retrieve_package(TEST_DB_PATH, id)

        create_dt = datetime.datetime.strptime(create_dt, DATETIME_FORMAT)

        self.assertEqual(package_id, id)
        self.assertEqual(package_data, ['string', 1.2, 5, True])
        self.assertAlmostEqual(create_dt, datetime.datetime.utcnow(),
                               delta=datetime.timedelta(seconds=1))
        self.assertEqual(device_id, 1)
        self.assertEqual(type_id, 1)

    def test_retrieve_packages(self):
        register_device(TEST_DB_PATH, 'my_device')
        register_package_type(TEST_DB_PATH, 'my_data_type',
                              SourceTypeEnum.JSON)
        store_package(TEST_DB_PATH, 1, 1, ('string', 1.2, 5, True))
        store_package(TEST_DB_PATH, 1, 1, ('whatever', 1.2, 5, True))

        records = retrieve_packages(TEST_DB_PATH, device_id=1)
        self.assertEqual(len(records), 2)

        records = retrieve_packages(TEST_DB_PATH, type_id=1)
        self.assertEqual(len(records), 2)

        records = retrieve_packages(TEST_DB_PATH)
        self.assertEqual(len(records), 2)


if __name__ == '__main__':
    unittest.main()
