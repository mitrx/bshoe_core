import unittest

from bshoe_core.logger import Logger
from bshoe_core.constants import LOGS_DIR
import os


class LoggerTestCase(unittest.TestCase):

    def setUp(self):
        self.logger_file_name = 'test'
        self.logger = Logger(self.logger_file_name)
        self.expected_logger_path = os.path.join(
            LOGS_DIR,
            self.logger_file_name + '.log'
        )

    def tearDown(self):
        self.logger.close()

    def test_init(self):
        self.assertNotEqual(self.logger, None)
        self.assertFalse(self.logger.first_time_opened)
        self.assertFalse(self.logger.manually_closed)
        self.assertEqual(self.logger.log_path, self.expected_logger_path)

    def test_after_log(self):
        log_message = 'my log message'
        self.logger.log(log_message)

        self.assertTrue(self.logger.first_time_opened)
        self.assertFalse(self.logger.manually_closed)

    def test_log_file_content_one_message(self):
        log_message = 'my log message'
        self.logger.log(log_message)

        self.logger.close()

        with open(self.expected_logger_path, 'r') as f:
            self.assertEqual(f.read(), log_message + '\n')

    def test_log_file_content_two_messages(self):
        log_message = 'my log message'
        another_log_message = 'my another log message'

        self.logger.log(log_message)
        self.logger.log(another_log_message)

        self.logger.close()

        with open(self.expected_logger_path, 'r') as f:
            self.assertEqual(
                f.read(),
                log_message + '\n' + another_log_message + '\n'
            )

    def test_close_opened(self):
        self.logger.log('')
        self.logger.close()
        self.assertTrue(self.logger.first_time_opened)
        self.assertTrue(self.logger.manually_closed)

    def test_close_not_opened(self):
        self.logger.close()
        self.assertFalse(self.logger.first_time_opened)
        self.assertFalse(self.logger.manually_closed)


if __name__ == '__main__':
    unittest.main()
