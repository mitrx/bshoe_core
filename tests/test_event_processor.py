import multiprocessing as mp
import unittest

from bshoe_core.event import MessageEvent, HaltReqEvent, HaltAckEvent

from bshoe_core.event_processor import EventProcessor


class EventProcessorTestCase(unittest.TestCase):

    def setUp(self):
        self.input_queue = mp.Queue()
        self.output_queue = mp.Queue()
        self.ep = EventProcessor(self.input_queue, self.output_queue)

    def tearDown(self):
        if self.ep.is_alive():
            self.ep.request_halt()
            self.ep.join()

    def test_simple_message(self):
        self.ep.start()
        input_event = MessageEvent('Hello', 'me', 'other')
        self.input_queue.put(input_event)

        response_event = self.output_queue.get()

        self.assertEqual(input_event.message, response_event.message)

    def test_thread_and_closable_lists(self):
        self.ep.start()
        self.assertEqual(3, len(self.ep._closables))
        self.assertEqual(1, len(self.ep._threads))
        self.ep.request_halt()
        self.ep.join()

    def test_termination_through_input_queue(self):
        self.ep.start()
        self.assertFalse(self.ep.terminated())

        halt_event = HaltReqEvent('me', 'other')
        self.input_queue.put(halt_event)

        output_event = self.output_queue.get()

        self.ep.join()

        self.assertIsInstance(output_event, HaltAckEvent)

        self.assertTrue(self.ep.terminated())

    def test_termination_through_request(self):
        self.ep.start()
        self.assertFalse(self.ep.terminated())

        self.ep.request_halt()
        self.ep.join()

        output_event = self.output_queue.get()

        self.assertIsInstance(output_event, HaltAckEvent)

        self.assertTrue(self.ep.terminated())

    def test_termination_before_start(self):
        self.assertRaises(RuntimeError, self.ep.request_halt)


if __name__ == '__main__':
    unittest.main()
