"""Module contains useful functions for the overall system"""
# from .constants import COMPONENTS


class IllegalComponentNameError(AttributeError):
    """Error class, which should be raised when invalid component name
    is given
    """
    pass


# def get_component_id(component_name):
#    """Returns component id given a component name"""
#    for comp_name, comp_id in COMPONENTS:
#        if component_name == comp_name:
#            return comp_id
#    raise IllegalComponentNameError()
