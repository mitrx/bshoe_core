"""Module contains a set of system constants"""
from enum import Enum
import os

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

LOGS_DIR_NAME = 'logs'
LOGS_DIR = os.path.join(ROOT_DIR, LOGS_DIR_NAME)

DB_DIR_NAME = 'db'
DB_DIR = os.path.join(ROOT_DIR, DB_DIR_NAME)

DB_FILE_NAME = 'device_data.db'
DB_PATH = os.path.join(DB_DIR, DB_FILE_NAME)

SQL_SCRIPTS_DIR_NAME = 'sql'
SQL_SCRIPTS_DIR = os.path.join(ROOT_DIR, SQL_SCRIPTS_DIR_NAME)

SQL_CREATE_SCRIPT_NAME = 'create.sql'
SQL_CREATE_SCRIPT = os.path.join(SQL_SCRIPTS_DIR, SQL_CREATE_SCRIPT_NAME)


class Component(Enum):
    EVENT_CLOUD = 'ec'
    REQUEST_REDIRECTOR = 'rr'
    MEMORY_MANAGER = 'mm'
    SCENARIO_MANAGER = 'sm'
    CONFIGURATION_MANAGER = 'cm'
    NETWORK_DELIVERY_SERVICE = 'nds'
