"""This module contains Logger class
"""

import os

from .constants import LOGS_DIR


class Logger(object):
    """Process which logs its actions to file"""
    def __init__(self, filename: str):
        super().__init__()
        self.log_path = os.path.join(LOGS_DIR, filename + '.log')
        self.first_time_opened = False
        self.manually_closed = False
        self.__log_file = None

    def log(self, message: str):
        """Writes message to the associated log file"""
        if not self.first_time_opened:
            self._open_log_file('w')
            self.first_time_opened = True
        elif self.__log_file.closed:
            print('REOPENING!')
            if self.manually_closed:
                raise RuntimeError('File is used after it was closed.')
            self._open_log_file('a')
        print(message, file=self.__log_file)
        self.__log_file.flush()

    def close(self):
        """Closes log file if it is open"""
        if self.__log_file is not None and not self.__log_file.closed:
            self._close_log_file()

    def _open_log_file(self, mode='a'):
        """Opens log file in specified mode"""
        self.__log_file = open(self.log_path, mode, encoding='utf-8')

    def _close_log_file(self):
        """Closes log file and marks it closed, so it can't be opened
        later on
        """
        self.__log_file.close()
        self.manually_closed = True
