from typing import Any

from bshoe_core.system import Requester


class StandardIORequestProvider(object):
    def __init__(self, requester: Requester):
        self.requester = requester

    def get_request(self):
        input_text = input('Request: ')
        self.requester.request(input_text)
