import sqlite3 as sqlite

from enum import Enum

from .constants import SQL_CREATE_SCRIPT

from typing import Union, Dict, List, Optional, Tuple, Any

import json


class SourceTypeEnum(Enum):
    STRING = 'string'
    INTEGER = 'integer'
    REAL = 'real'
    JSON = 'json'
    BINARY = 'binary'


SOURCE_TYPES = [t.value for t in SourceTypeEnum]

BaseType = Union[str, int, float, bool, None]
Json = Union[BaseType, Dict[BaseType, 'Json'],
             List['Json'], Tuple['Json', ...]]
PackageData = Union[bytes, BaseType, Json]
PackageRecord = Tuple[int, PackageData, str, int, int]
Id = Union[int, str]
DeviceRecord = Tuple[int, str]


def init(db_path: str):
    """Initializes a device packges storage database given its path."""
    with sqlite.connect(db_path) as db_conn:
        with open(SQL_CREATE_SCRIPT, 'r', encoding='utf-8') as script:
            db_conn.executescript(script.read())


def register_device(db_path: str, device_name: str) -> int:
    """Creates an instance of device in the database."""
    if not isinstance(device_name, str):
        raise_type_error(device_name, 'device_name')

    with sqlite.connect(db_path) as db_conn:
        cursor = db_conn.execute('INSERT INTO device(device_name)'
                                 'VALUES (?)', (device_name, ))
        device_id = cursor.lastrowid

    return device_id


SourceType = Union[str, SourceTypeEnum]


def register_package_type(db_path: str, type_name: str,
                          source_type: SourceType) -> int:
    """Creates an instance of package type."""
    if not isinstance(type_name, str):
        raise_type_error(type_name, 'type_name')

    if isinstance(source_type, SourceTypeEnum):
        source_type = source_type.value
    elif isinstance(source_type, str):
        if source_type not in SOURCE_TYPES:
            raise AttributeError('The value of source type is invalid')
    else:
        raise_type_error(source_type, 'source_type')

    with sqlite.connect(db_path) as db_conn:
        cursor = db_conn.execute(
            'INSERT INTO package_type(type_name, source_type)'
            'VALUES (?, ?)', (type_name, source_type)
        )
        package_type_id = cursor.lastrowid

    return package_type_id


Device = Union[int, str]  # device name or device id
PackageType = Union[int, str]  # package type name or id


def store_package(db_path: str, device: Device, package_type: PackageType,
                  package_data: PackageData) -> int:
    """Stores a data package of a device with a specified type.
    db_path --- location of the database in the system
    device --- either device name of its id
    package_type --- either package type name or its id
    package_data --- either any object which can be converted by json.dumps
    to a string or bytes.
    """
    (device_id, device_name) = retrieve_device(db_path, device)
    (type_id, type_name,
     source_type) = retrieve_package_type(db_path, package_type)

    check_source_type_conformance(package_data, source_type)

    if isinstance(package_data, bytes):
        # keep bytes data as-is
        pass
    else:
        # delegate conversion to int, real, string to json.dumps and
        # let the json.dumps raise errors in data object structure
        try:
            package_data = json.dumps(package_data, separators=(',', ':'))
        except:
            raise ValueError('given package object could not be'
                             'converted to a string. Obeject has'
                             'invalid structure.')

    with sqlite.connect(db_path) as db_conn:
        cursor = db_conn.execute(
            ('INSERT INTO data_package(device_id, type_id, package_data)'
             'VALUES (?, ?, ?)'), (device_id, type_id, package_data)
        )
        package_id = cursor.lastrowid

    return package_id


def check_source_type_conformance(package, source_type):
    if source_type == SourceTypeEnum.BINARY.value:
        assert isinstance(package, bytes)
    elif source_type == SourceTypeEnum.INTEGER.value:
        assert isinstance(package, int)
    elif source_type == SourceTypeEnum.REAL.value:
        assert isinstance(package, float)
    elif source_type == SourceTypeEnum.STRING.value:
        assert isinstance(package, str)
    elif source_type == SourceTypeEnum.JSON.value:
        assert isinstance(package, (list, tuple, dict))
    else:
        raise ValueError('the source type of specified type does not'
                         'conform with package content')


def retrieve_device(db_path: str, device_id: Id) -> Optional[DeviceRecord]:
    """Returns a record of device as it is stored in db."""
    if isinstance(device_id, str):
        sql = 'SELECT * FROM device WHERE device_name = ?'
    elif isinstance(device_id, int):
        sql = 'SELECT * FROM device WHERE device_id = ?'
    else:
        raise_type_error(device_id, 'device_id')

    with sqlite.connect(db_path) as db_conn:
        cursor = db_conn.execute(sql, (device_id, ))

        device = cursor.fetchone()

    return device


PackageType = Tuple[int, str, str]


def retrieve_package_type(db_path: str, type_id: Id) -> Optional[PackageType]:
    """Returns a record as it is stored in db."""
    if isinstance(type_id, str):
        sql = 'SELECT * FROM package_type WHERE type_name = ?'
    elif isinstance(type_id, int):
        sql = 'SELECT * FROM package_type WHERE type_id = ?'
    else:
        raise_type_error(type_id, 'type_id')

    with sqlite.connect(db_path) as db_conn:
        cursor = db_conn.execute(sql, (type_id, ))
        package_type = cursor.fetchone()

    return package_type


def retrieve_package(db_path: str, package_id: Id) -> PackageRecord:
    """Returns a record of package with unpacked package_data field."""
    with sqlite.connect(db_path) as db_conn:
        cursor = db_conn.execute('SELECT * FROM data_package '
                                 'WHERE package_id = ?', (package_id, ))

        package_record = cursor.fetchone()

    package_type_id = package_record[4]
    (_, _, source_type) = retrieve_package_type(db_path, package_type_id)
    return decode_data_package(package_record, source_type)


def retrieve_packages(db_path: str, device_id: Optional[Id] = None,
                      type_id: Optional[Id] = None,
                      create_dt: Optional[str] = None) -> List[PackageRecord]:
    """Returns the list of records containing packages."""
    if device_id is not None:
        (_, device_name) = retrieve_device(db_path, device_id)

    if type_id is not None:
        (_, type_name, source_type) = retrieve_package_type(db_path, type_id)

    (sql_condition, args) = make_package_condition(device_id, type_id,
                                                   create_dt)

    sql = ('SELECT data_package.*, package_type.source_type '
           'FROM data_package, package_type WHERE'
           ' package_type.type_id = data_package.type_id')
    if sql_condition is not None and sql_condition != '':
        sql += ' AND ' + sql_condition

    with sqlite.connect(db_path) as db_conn:
        cursor = db_conn.execute(sql, args)
        packages = [decode_data_package(record, record[5])
                    for record in cursor]

    return packages


def decode_data_package(record: PackageRecord, source_type: SourceTypeEnum):
    """Returns a package record with unpacked data field."""
    if source_type == SourceTypeEnum.BINARY.value:
        decoded_data = record[1]
    else:
        decoded_data = json.loads(record[1])

    return record[0], decoded_data, record[2], record[3], record[4]


def delete_package(db_path: str, package_id: Id) -> bool:
    """Deletes record with the specified package_id. Returns true if package
    with specified package_id is removed.
    """
    with sqlite.connect(db_path) as db_conn:
        cursor = db_conn.cursor()
        cursor.execute('DELETE FROM data_package '
                       'WHERE package_id = ?', (package_id, ))
        deleted_rows_count = cursor.rowcount

    return deleted_rows_count == 1


def delete_packages(
        db_path: str,
        device_id: Optional[Union[str, int]] = None,
        package_type: Optional[Union[str, int]] = None,
        create_datetime: Optional[Union[str, Tuple[str, str]]] = None
) -> int:
    """Deletes packages which satisfy parameter constraints.
    Returns the number of deleted packages."""
    (sql_condition, args) = make_package_condition(device_id, package_type,
                                                   create_datetime)

    if sql_condition == '':
        return 0

    with sqlite.connect(db_path) as db_conn:
        cursor = db_conn.execute(
            'DELETE FROM data_package WHERE ' + sql_condition,
            args
        )
        deleted_rows_count = cursor.rowcount

    return deleted_rows_count


CreateDateTime = Union[str, Tuple[str, str]]


def make_package_condition(
        device_id: Optional[Id] = None,
        package_type_id: Optional[Id] = None,
        create_dt: Optional[CreateDateTime] = None) -> Tuple[str, Tuple[Any]]:
    """Combines and returns a condition string for package entity
    related query.
    """
    conditions = []
    args = []

    if device_id is not None:
        conditions.append('data_package.device_id = ?')
        args.append(device_id)

    if package_type_id is not None:
        conditions.append('data_package.type_id = ?')
        args.append(package_type_id)

    if isinstance(create_dt, tuple):
        conditions.append('data_package.create_datetime BETWEEN ? AND ?')
        args.append(create_dt[0])
        args.append(create_dt[1])
    elif create_dt is not None:
        conditions.append('data_pacakge.create_datetime = ?')
        args.append(create_dt)

    return ' AND '.join(conditions), tuple(args)


def raise_type_error(actual_param: Any, param_name: str) -> None:
    """Raises a TypeError with a given parameter name and actual
    required parameter.
    """
    raise TypeError("Object of type '{}' is not acceptable as {} parameter"
                    .format(type(actual_param), param_name))
