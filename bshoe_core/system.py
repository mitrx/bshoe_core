#!/usr/bin/env python
"""Module composes all the components of the system"""
import os
from multiprocessing import Pipe, Queue
from multiprocessing.connection import Connection

from typing import Any

# from .configuration_manager import ConfigurationManager
from .constants import DB_DIR, DB_PATH, LOGS_DIR, Component
from .db_utils import init as init_db
from .event_cloud import EventCloud
from .memory_manager import MemoryManager
# from .network_delivery_service import NetDeliveryService
from .request_redirector import RequestRedirector
# from .scenario_manager import ScenarioManager
from .standard_io_request_provider import StandardIORequestProvider


class Requester(object):
    def __init__(self, connection: Connection):
        self.connection = connection

    # TODO: replace with actual request types
    def request(self, request: Any) -> Any:
        """Sends message to the system"""
        self.connection.send(request)
        # return self.connection.recv()


class Bshoe(object):
    """This class responsibility is to compose all the system components
    and orchestrate them
    """

    def __init__(self, request_provider):
        self.queues = {
            Component.EVENT_CLOUD.value: Queue(),
            Component.REQUEST_REDIRECTOR.value: Queue(),
            Component.MEMORY_MANAGER.value: Queue()
            # Component.SCENARIO_MANAGER.value: Queue(),
            # Component.CONFIGURATION_MANAGER.value: Queue(),
            # Component.NETWORK_DELIVERY_SERVICE.value: Queue()
        }
        self.processes = []

        self.external_conn, internal_conn = Pipe()

        self.rr_process = RequestRedirector(
            self.queues[Component.REQUEST_REDIRECTOR.value],
            self.queues[Component.EVENT_CLOUD.value],
            internal_conn,
            'rr'
        )
        self.processes.append(self.rr_process)

        self.mm_process = MemoryManager(
            self.queues[Component.MEMORY_MANAGER.value],
            self.queues[Component.EVENT_CLOUD.value],
            DB_PATH
        )
        self.processes.append(self.mm_process)

        # self.sm_process = ScenarioManager(
        #     self.queues[Component.SCENARIO_MANAGER.value],
        #     self.queues[Component.EVENT_CLOUD.value]
        # )
        # self.processes.append(self.sm_process)

        # self.cm_process = ConfigurationManager(
        #     self.queues[Component.CONFIGURATION_MANAGER.value],
        #     self.queues[Component.EVENT_CLOUD.value]
        # )
        # self.processes.append(self.cm_process)

        # self.nds_process = NetDeliveryService(
        #     self.queues[Component.NETWORK_DELIVERY_SERVICE.value],
        #     self.queues[Component.EVENT_CLOUD.value]
        # )
        # self.processes.append(self.nds_process)

        self.ec_process = EventCloud(
            self.queues[Component.EVENT_CLOUD.value],
            self.queues
        )
        self.processes.append(self.ec_process)

        self.requester = Requester(self.external_conn)
        self.request_provider = request_provider

    def run(self):
        """Launches execution of components of the system"""
        for process in self.processes:
            process.start()

        requester = self.requester

        while True:
            request = self.request_provider.get_request()
            if isinstance(request, int) and request == -1:
                requester.request('exit')
                break
            requester.request(request)

        # map(lambda p: p.start(), self.processes)
        # map(lambda p: p.join(), self.processes)
        for process in self.processes:
            process.join()
        self.external_conn.close()
        return self


def init():
    if not os.path.exists(LOGS_DIR):
        os.mkdir(LOGS_DIR)

    if not os.path.exists(DB_DIR):
        os.mkdir(DB_DIR)

    init_db(DB_PATH)


def run():
    bshoe = Bshoe(StandardIORequestProvider())
    bshoe.run()


if __name__ == '__main__':
    run()
