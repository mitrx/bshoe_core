"""Module contains ScenarioManager component of the system"""
import multiprocessing as mp
from typing import Optional
import time

from .event_processor import EventProcessor
from .constants import Component, DEVICE_CONFIG_DIR
from .event import ScenarioLaunchEvent, StoreDataPackageReq

from drivers.camera_driver import CameraDriver


class ScenarioManager(EventProcessor):
    """Component of the system whose responsibility if to manage execution
    scenarios in the system"""
    COMPONENT_NAME = Component.SCENARIO_MANAGER.value

    def __init__(self,
                 in_queue: mp.Queue,
                 out_queue: mp.Queue,
                 name: Optional[str] = COMPONENT_NAME):
        super(ScenarioManager, self).__init__(in_queue, out_queue,
                                              name=name)
        self._set_event_handler(self.__handle_scenario_launch_event,
                                ScenarioLaunchEvent)

    def __handle_scenario_launch_event(self, event: ScenarioLaunchEvent):
        print('scenario launch request have arrived')
        if event.scenario_id == 1:
            camera_driver = CameraDriver(DEVICE_CONFIG_DIR, 1)
            start_time = time.time()
            image = camera_driver.fetch_image()
            execution_time = time.time() - start_time
            self._log('image fetched for {}s'.format(execution_time))

            return StoreDataPackageReq(
                'camera-1',
                'package',
                image,
                self.name
            )

            # return MessageEvent('image is downloaded',
            #                     Component.SCENARIO_MANAGER.value,
            #                     Component.REQUEST_REDIRECTOR.value)
