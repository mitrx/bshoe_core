"""Module contains ScenarioManager component of the system"""
from .event_processor import EventProcessor


class ScenarioManager(EventProcessor):
    """Component of the system whose responsibility if to manage execution
    scenarios in the system"""
    pass
