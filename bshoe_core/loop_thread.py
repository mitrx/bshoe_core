"""
    Module contains class LoopThread, which represents infinite loop
    with an action on each step
"""
import threading as th
from typing import Callable, Any, Optional


class LoopThread(th.Thread):
    """
        Represents a thread which is an infinite loop which executes a
        step function on each iteration
    """

    def __init__(self, step_function: Callable[..., Any],
                 name: Optional[str] = None):
        super().__init__()

        self.name = name

        self._can_run = th.Event()
        self._can_run.set()

        self._step_done = th.Event()
        self._step_done.clear()

        self._must_terminate = th.Event()
        self._must_terminate.clear()

        self._step_function = step_function

    def pause(self):
        """Pauses the thread before the the next iteration of the loop"""
        if self.is_alive():
            self._can_run.clear()
            self._step_done.wait()

    def resume(self):
        """Resumes the process after it was paused"""
        if self.is_alive():
            self._can_run.set()

    def request_termination(self):
        """Sets terminate flag, such that the thread terminates on its
        next iteration
        """
        if self._started.is_set():
            self._must_terminate.set()
            if not self.is_running():
                self.resume()
        else:
            raise RuntimeError("can't terminate, the thread is not started")

    def is_paused(self) -> bool:
        """Returns True if the thread was paused after it was launched"""
        return not self._can_run.is_set() and self._step_done.is_set()

    def is_running(self) -> bool:
        """Returns True if the thread was launched and is executing"""
        return (self._can_run.is_set() and self._started.is_set() and
                self.is_alive())

    def run(self):
        """Run the execution loop, which executes step_function"""
        self._can_run.wait()
        while not self._must_terminate.is_set():
            try:
                # clearing the flag that indicates that step function is done
                self._step_done.clear()
                self._step_function()
            finally:
                # setting the flag after the step function is done
                self._step_done.set()
            self._can_run.wait()
        # print(self.name, 'terminated')
