"""Module contains event cloud component of the system"""
import multiprocessing as mp

from typing import Dict, Optional

from .event import Event, HaltReqEvent, HaltAckEvent
from .event_processor import EventProcessor
from .constants import Component


class EventCloud(EventProcessor):
    """This class is represents a process which is event mediator between
    processes of Bshoe system
    """
    COMPONENT_NAME = Component.EVENT_CLOUD.value

    def __init__(self, input_queue: mp.Queue,
                 output_queues: Dict[str, mp.Queue],
                 name: Optional[str] = COMPONENT_NAME):
        super(EventCloud, self).__init__(input_queue, name=name)

        self._out_queues = output_queues.copy()
        if self.COMPONENT_NAME in self._out_queues:
            del self._out_queues[Component.EVENT_CLOUD.value]

        assert self.COMPONENT_NAME not in self._out_queues

        self._set_event_handler(self.__handle_halt_request, HaltReqEvent)
        self._set_event_handler(self.__handle_halt_ackowledge, HaltAckEvent)

        for _, closable in output_queues.items():
            self._add_closable(closable)

    def _send_event(self, event: Event):
        """Puts event to a queue according to its on the recipient"""
        if event.recipient is None:
            for _, queue in self._out_queues.items():
                queue.put(event)
        else:
            self._get_outgoing_queue(event.recipient).put(event)

    def _get_outgoing_queue(self, recipient: str = None) -> mp.Queue:
        """Returns the queue associated to recipient"""
        return self._out_queues[recipient]

    # def _handle_event(self, event: Event) -> Optional[Event]:
    #     try:
    #         self._log('arrived event with content "{}"'.format(event.message))
    #         self._log('sending event with text "{}"'.format(event.message))
    #         return event
    #     except (EOFError, OSError):
    #         self._log('event_cloud should\'ve termiante here')
    #         # I should terminate myself here
    #         # self.terminate()

    def __handle_halt_request(self, event: HaltReqEvent) -> HaltReqEvent:
        if isinstance(event, HaltReqEvent):
            self.components_terminated = len(self._out_queues)
            return HaltReqEvent(self.name)

    def __handle_halt_ackowledge(self, event: HaltAckEvent):
        self.components_terminated -= 1
        if self.components_terminated == 0:
            self._request_threads_termination()

    def _default_event_handler(self, event: Event) -> Event:
        return event
