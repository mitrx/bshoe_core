"""Module contains NetworkDeliveryService component of the system"""
from .event_processor import EventProcessor


class NetDeliveryService(EventProcessor):
    """This class is responsible to deliver data to remote server"""
    pass
