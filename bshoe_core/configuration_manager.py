"""
Module contains class ConfigurationManager which represents component
of the system
"""
from .event_processor import EventProcessor


class ConfigurationManager(EventProcessor):
    pass
