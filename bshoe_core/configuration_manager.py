"""
Module contains class ConfigurationManager which represents component
of the system
"""
import multiprocessing as mp
from typing import Optional

from .event_processor import EventProcessor
from .constants import Component


class ConfigurationManager(EventProcessor):
    COMPONENT_NAME = Component.CONFIGURATION_MANAGER.value

    def __init__(self,
                 in_queue: mp.Queue,
                 out_queue: mp.Queue,
                 name: Optional[str] = COMPONENT_NAME):
        super(ConfigurationManager, self).__init__(in_queue, out_queue,
                                                   name=name)
