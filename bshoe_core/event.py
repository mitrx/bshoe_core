"""This module contains definition of base class for events in the system
"""
from typing import Optional


class Event(object):
    """Base class of event"""
    def __init__(self, message: str,
                 sender: Optional[str] = None,
                 recipient: Optional[str] = None):
        self.message = message
        self.sender = sender
        self.recipient = recipient


class HaltReqEvent(Event):
    """Class represents request-event to require halt of the system"""
    def __init__(self, sender: Optional[str] = None,
                 recipient: Optional[str] = None):
        """Creates instance of request. If recipient is not specified, then
        the event is a broadcast
        """
        super(HaltReqEvent, self).__init__('HALT_REQ', sender, recipient)


class HaltAckEvent(Event):
    """Class represents response-event to acknowledge halt of the system"""
    def __init__(self, sender: Optional[str] = None,
                 recipient: Optional[str] = None):
        super(HaltAckEvent, self).__init__('HALT_ACK', sender, recipient)

class MessageEvent(Event):
    def __init__(self, message: str,
                 sender: Optional[str] = None,
                 recipient: Optional[str] = None):
        super(MessageEvent, self).__init__(message, sender, recipient)
