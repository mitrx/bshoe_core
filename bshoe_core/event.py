"""This module contains definition of base class for events in the system
"""
from typing import Optional, Any
from .constants import Component


class Event(object):
    """Base class of event"""
    def __init__(self, name: str,
                 sender: Optional[str] = None,
                 recipient: Optional[str] = None):
        self.name = name
        self.sender = sender
        self.recipient = recipient


class HaltReqEvent(Event):
    """Class represents request-event to require halt of the system"""
    def __init__(self, sender: Optional[str] = None,
                 recipient: Optional[str] = None):
        """Creates instance of request. If recipient is not specified, then
        the event is a broadcast
        """
        super(HaltReqEvent, self).__init__('HALT_REQ', sender, recipient)


class HaltAckEvent(Event):
    """Class represents response-event to acknowledge halt of the system"""
    def __init__(self, sender: Optional[str] = None,
                 recipient: Optional[str] = None):
        super(HaltAckEvent, self).__init__('HALT_ACK', sender, recipient)


class MessageEvent(Event):
    def __init__(self, message: str,
                 sender: Optional[str] = None,
                 recipient: Optional[str] = None):
        super(MessageEvent, self).__init__(message, sender, recipient)
        self.message = message


class ScenarioLaunchEvent(Event):
    def __init__(self, scenario_id: int,
                 sender: Optional[str] = None,
                 recipient: Optional[str] = None):
        super(ScenarioLaunchEvent, self).__init__('SCENARIO_LAUNCH', sender,
                                                  recipient)
        self.scenario_id = scenario_id


class StoreDataPackageReq(Event):
    def __init__(self, device: str, package_type: str, data: Any, sender: str):
        super(StoreDataPackageReq, self).__init__(
            'StoreDataPackageReq',
            sender,
            Component.MEMORY_MANAGER.value
        )
        self.device = device
        self.package_type = package_type
        self.data = data


class GetDataPackageReq(Event):
    def __init__(self, device: str, package_type: str, package_id: str, sender):
        super(GetDataPackageReq, self).__init__('GetDataPackageReq', sender,
                                                Component.MEMORY_MANAGER.value)
        self.device = device
        self.package_type = package_type
        self.package_id = package_id

class GetDataPackageRes(Event):
    def __init__(self, recipient: str, package_id: int, package_data: Any,
                 create_dt: str, device_id: int, type_id: int):
        super(GetDataPackageRes, self).__init__('GetDataPackageRes',
                                             Component.MEMORY_MANAGER.value,
                                             recipient)
        self.package_id = package_id
        self.data = package_data
        self.create_datetime = create_dt
        self.device_id = device_id
        self.type_id = type_id
