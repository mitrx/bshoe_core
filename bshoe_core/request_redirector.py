"""Contains RequestRedirector component of the system"""
from typing import Optional

from .event import Event, HaltReqEvent, MessageEvent
from .event_processor import EventProcessor
from .loop_thread import LoopThread
from .constants import Component

from multiprocessing import Queue
from multiprocessing.connection import Connection

import threading as th


class RequestRedirector(EventProcessor):
    """Represents a process which takes external requests and
    transforms them into Bshoe System Core events
    """
    EXIT_CMD = 'exit'
    COMPONENT_NAME = Component.REQUEST_REDIRECTOR.value

    def __init__(self, in_queue: Queue,
                 out_queue: Queue,
                 external_connection: Optional[Connection] = None,
                 name: Optional[str] = COMPONENT_NAME):
        super().__init__(in_queue, out_queue, name=name)

        if external_connection is not None:
            self._external_events = external_connection
            self._add_closable(external_connection)
            self._add_thread(LoopThread(
                self._event_pusher,
                name='{}({})'.format(name, 'event_pusher')
            ))

    def _event_pusher(self):
        """Pushes event received from outside to the event cloud"""
        try:
            event_text = self._external_events.recv()
            # 'exit' means that the user wants to terminate
            if event_text == RequestRedirector.EXIT_CMD:
                self._log('"exit" command is arrived')
                self._log('terminating {} thread...'.
                          format(th.current_thread().name))

                self._request_threads_termination()
                # self.request_halt()
                self._send_event(HaltReqEvent(self.name))
            else:
                self._log('sending event with text "' + event_text + '"')
                event = MessageEvent(event_text,
                                     Component.REQUEST_REDIRECTOR.value,
                                     Component.MEMORY_MANAGER.value)
                self._out_events.put(event)
                self._log('event with text "{}" is sent'
                          .format(event.message))
        except EOFError:
            pass

    def _pop_event(self, event: Event) -> Optional[Event]:
        """Processes event and returns response event to the system or None
        if there is no response event"""
        self._log('process_event started!')
        self._log('waiting for message...')
        event = self._incoming_queue().get()
        self._log('arrived event with content "{}"'.format(event.message))
        self._external_events.send(event.message)

    def __handle_message_event(self, event: MessageEvent) -> None:
        self._log('Received message: {}'.format(event.message))
