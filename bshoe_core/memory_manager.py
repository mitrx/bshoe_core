"""This module contains Memory Manager component of the system"""
import multiprocessing as mp

from .event_processor import EventProcessor

# from .db_utils import (
#     register_device, register_package_type, store_package, retrieve_device,
#     retrieve_package_type, retrieve_package, retrieve_packages,
#     delete_package, delete_packages, SourceTypeEnum
# )

from .event import Event, HaltReqEvent, HaltAckEvent, MessageEvent

from typing import Optional

from .constants import Component


class MemoryManager(EventProcessor):
    """Memory manager is the process which receives events related to reading
    and writing from persistent storage data related to a device
    """
    COMPONENT_NAME = Component.MEMORY_MANAGER.value

    def __init__(self,
                 in_queue: mp.Queue,
                 out_queue: mp.Queue,
                 db_path: str,
                 name: Optional[str] = COMPONENT_NAME):
        super(MemoryManager, self).__init__(in_queue, out_queue,
                                            name=name)
        self.db_path = db_path
        self._set_event_handler(self.__handle_event, Event)
        self._set_event_handler(self.__handle_message_event, MessageEvent)

    def __handle_event(self, event: Event) -> Optional[Event]:
        print('processed')

    def __handle_message_event(self, event: MessageEvent) -> MessageEvent:
        return MessageEvent('processed by MemoryManager', self.name,
                            event.sender)
