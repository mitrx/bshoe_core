"""This module contains Memory Manager component of the system"""
import multiprocessing as mp

from .event_processor import EventProcessor

from .event import (Event, HaltReqEvent, HaltAckEvent, MessageEvent,
                    StoreDataPackageReq, GetDataPackageReq, GetDataPackageRes)

from typing import Optional

from .constants import Component

from .db_utils import (store_package, register_device, register_package_type,
                       retrieve_package)


class MemoryManager(EventProcessor):
    """Memory manager is the process which receives events related to reading
    and writing from persistent storage data related to a device
    """
    COMPONENT_NAME = Component.MEMORY_MANAGER.value

    def __init__(self,
                 in_queue: mp.Queue,
                 out_queue: mp.Queue,
                 db_path: str,
                 name: Optional[str] = COMPONENT_NAME):
        super(MemoryManager, self).__init__(in_queue, out_queue,
                                            name=name)
        self.db_path = db_path
        self._set_event_handler(self.__handle_event, Event)
        self._set_event_handler(self.__handle_message_event, MessageEvent)
        self._set_event_handler(self.__handle_store_data_package,
                                StoreDataPackageReq)
        self._set_event_handler(self.__handle_get_data_package,
                                GetDataPackageReq)

    def __handle_event(self, event: Event) -> Optional[Event]:
        pass

    def __handle_message_event(self, event: MessageEvent) -> MessageEvent:
        return MessageEvent('processed by MemoryManager', self.name,
                            event.sender)

    def __handle_store_data_package(self, event: StoreDataPackageReq):
        store_package(self.db_path, event.device, event.package_type,
                      event.data)

    def __handle_get_data_package(self, event: GetDataPackageReq):
        record = retrieve_package(self.db_path, event.package_id)
        return GetDataPackageRes(event.sender, *record)
