import multiprocessing as mp
import threading as th

from typing import Any, Optional, Callable

from .event import Event, HaltAckEvent, HaltReqEvent, MessageEvent
from .logger import Logger
from .loop_thread import LoopThread


class EventProcessor(mp.Process):
    def __init__(self, in_queue: mp.Queue,
                 out_queue: Optional[mp.Queue] = None,
                 name: Optional[str] = 'ep'):

        super(EventProcessor, self).__init__(
            name=name,
            args=(in_queue, out_queue)
        )
        self.name = name

        self._closables = []
        self._threads = []

        self._logger = Logger(name if name is not None
                              else self.__class__.__name__)
        self._add_closable(self._logger)

        self._in_events = in_queue
        self._add_closable(in_queue)

        if out_queue is not None:
            self._out_events = out_queue
            self._add_closable(out_queue)

        self._event_handlers = {}
        self._set_event_handler(self.__default_halt_request_handler,
                                HaltReqEvent)
        self._set_event_handler(self.__default_event_handler,
                                Event)
        self._set_event_handler(self.__default_message_event_handler,
                                MessageEvent)

        self._add_thread(LoopThread(
            self._event_processor,
            name='{}({})'.format(name, 'event_processor')
        ))
        self._started = False

    def start(self):
        super().start()
        self._started = True

    def run(self):
        """Launches execution of the process"""
        self._log('started')
        try:
            for thread in self._threads:
                thread.start()
            for thread in self._threads:
                thread.join()
                del thread
            del self._threads
        finally:
            self._log('terminated')
            self._close_closables()

    def request_halt(self):
        """Sends HaltReqEvent to current process, forcing it to graceful
        termination. Must be called only by parent process.
        """
        if not self._started:
            raise RuntimeError(
                "Can't request termination of non-started process."
                " Maybe you tried to terminate process from itself"
            )

        self._incoming_queue().put(HaltReqEvent())

    def terminated(self) -> bool:
        """Returns true of the process is terminated"""
        return self.exitcode is not None

    def _send_event(self, event: Event):
        """Sends an event to the output event queue"""
        self._get_outgoing_queue().put(event)

    def _receive_event(self) -> Optional[Event]:
        """Returns an event from the input event queue"""
        return self._incoming_queue().get()

    def _request_threads_termination(self):
        """Gracefully terminates the process by terminating all its threads"""
        # at this point the code is asynchronous
        # either threads or queues will close first
        for thread in self._threads:
            thread.request_termination()

    def _add_thread(self, thread: th.Thread):
        """Adds a thread to the pool of the threads"""
        self._threads.append(thread)

    def _add_closable(self, closable):
        """Adds a closable object to the pool of closables"""
        if 'close' in dir(closable):
            self._closables.append(closable)
        else:
            raise Exception("can't add not a closable object")

    def _remove_closable(self, closable: Any):
        """Removes closable from the pool of closables if it is in the pool,
        otherwise throws an error"""
        self._closables.remove(closable)

    def _close_closables(self):
        """Closes all the closables if there are opened"""
        for closable in self._closables:
            try:
                closable.close()
            except OSError:
                pass  # the closable is already closed

    def _incoming_queue(self) -> mp.Queue:
        """Returns the input event queue"""
        return self._in_events

    def _get_outgoing_queue(self, recipient: Optional[str] = None) -> mp.Queue:
        """Returns input event queue of a recipient"""
        return self._out_events

    def _log(self, message: str):
        """Logs a message to the associated logger"""
        self._logger.log(message)

    def _event_processor(self):
        """Endlessly iterates taking events from associated input queue,
        processing them and pushing result to the output queue, until
        HaltReqEvent arrives
        """
        self._log('waiting for message...')
        try:
            event = self._receive_event()
        except (EOFError):
            self._request_threads_termination()
            return

        if event is None:
            return
        self._log('received event with message "{}"'
                  .format(event.message))
        response_event = self._handle_event(event)

        if response_event is not None:
            self._send_event(response_event)
            self._log('event with text "{}" is sent'.format(
                response_event.message))

        # if isinstance(response_event, HaltAckEvent):
        #     self._log('terminating...')
        #     self._request_threads_termination()

    # def _process_event(self, event: Event) -> Optional[Event]:
    #     """Processes event and returns event in response"""
    #     if isinstance(event, HaltReqEvent):
    #         return HaltAckEvent(event.recipient, event.sender)

    #     response_text = '[processed by {}] {}' \
    #         .format(self.__class__.__name__, event.message)

    #     return Event(response_text, event.recipient, event.sender)

    def _set_event_handler(self, handler: Callable, event_class: type):
        if not issubclass(event_class, Event):
            raise AttributeError('invalid type of event_class parameter ({})'.
                                 format(event_class))
        self._event_handlers[event_class.__name__] = handler

    def _handle_event(self, event: Event) -> Optional[Event]:
        # Calls an event from dictionary of handlers and returns its response
        # if there is one
        if not isinstance(event, Event):
            raise AttributeError('invalid type of event parameter ({})'.
                                 format(type(event)))
        event_type_name = type(event).__name__
        if event_type_name in self._event_handlers:
            return self._event_handlers[event_type_name](event)
        else:
            return self._default_event_handler(event)

    def __default_halt_request_handler(self, e: HaltReqEvent) -> None:
        self._request_threads_termination()
        return HaltAckEvent(mp.current_process().name)

    def __default_event_handler(self, event: Event) -> Optional[Event]:
        pass

    def __default_message_event_handler(self, e: MessageEvent) -> MessageEvent:
        return MessageEvent(e.message, e.recipient, e.sender)
